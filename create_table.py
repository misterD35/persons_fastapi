import psycopg2


connection = 0
try:
    connection = psycopg2.connect(  # данные для подключения к базе данных
        host='localhost',
        user='postgres',
        password='123',
        database='persons_info',
    )

    connection.autocommit = True    # метод для внесения изменений в БД


    with connection.cursor() as cursor:        # подключение к методу курсор для работы команд
        cursor.execute(
            """CREATE TABLE persons(
                id serial NOT NULL PRIMARY KEY,
                name varchar NOT NULL,
                gender varchar NOT NULL);"""
        )

        print("[INFO] Table created successfully")


except Exception as _ex:
    print()
    print("[INFO] Error while working with PostgreSQL", _ex)
finally:
    if connection:
        connection.close()
        print()
        print("[INFO] PostgreSQL connection closed")
