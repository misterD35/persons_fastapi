# Persons_FastApi

[![Generic badge](https://img.shields.io/badge/Python-3.10-green.svg)](https://www.python.org/) 


Hello! :hand_splayed: Here you can see a small project on ***FastApi*** and ***PostgreSQL***.
***

## Description

This project is based on ***Python***. It connects with framework ***FastApi*** and database ***PostgreSQL*** (*psycopg2*). 

It has 3 options: 
* get data of all persons from database
* get data of with the ability to filter by gender, with a specified LIMIT
* add (*post*) new data about persons.

## Installation
After you have added the files to your repository go to your terminal and run command 
```
pip install -r requirements.txt
``` 
Then do some actions which listed below:

1) Open file *config.py* and set your database options (host, user, password, db_name)
2) Start file *create_table.py*. Program will create table in your database with 3 columns (id, name, gender).
    * If you want to work with example data insert it by running file *insert_example_data.py*.
    * If you prefer to work without example data and plan to add your one, ignore file *insert_example_data.py*
3) Run file *Persons.py* and look at your terminal. There you can see a link. Click it and go to webserver.

Example of ***main URL*** is `http://127.0.0.1:8000`


## Usage
Below are the available webserver features:

* Add new person data to your database with method *post*. 
    *  go to ***main URL*** + '/new_person' + 
'?name=Example_name&gender=str'. In the end you have something like this `http://127.0.0.1:8000/new_person?name=Jasper&gender=m`. Name can be anything, gender can be 'm' or 'f' (*male or female*)
* Get data of all persons from database.
    * go to ***main URL*** + '/persons/all'. In the end you have something like this `http://127.0.0.1:8000/persons/all`.
* Get data of with the ability to filter by gender, with a specified LIMIT
    * go to ***main URL*** + '/persons/some_person' + '?gender=str&limit=num'. In the end you have something like this `http://127.0.0.1:8000/persons/some_person?gender=f&limit=4`.

## Technologies
![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)   ![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)    
- [x] [Python](https://www.python.org/) 3.10  :snake: 
- [x] [PostgreSQL](https://www.postgresql.org/) 15.1   :elephant:

___

## License
Apache-2.0

<img src="https://cdn-icons-png.flaticon.com/512/919/919852.png?w=740&t=st=1674815666~exp=1674816266~hmac=d8675dffadfc01e1bb6ec97b220b11c5e004da72099526ebc7569461b3b0ce53" width="150" height="150" >   <img src="https://cdn.worldvectorlogo.com/logos/postgresql.svg" width="250" height="150" >


