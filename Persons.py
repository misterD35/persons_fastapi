import psycopg2 as psycopg2
import uvicorn
from fastapi import FastAPI, Query
from pydantic import BaseModel

app = FastAPI()
connection = psycopg2.connect(  # данные для подключения к базе данных
    host='localhost',
    user='postgres',
    password='123',
    database='staff_info',
)


class Person(BaseModel):
    id: int
    name: str
    gender: str


@app.get('/persons/all')
def get_all_person_data():
    response = []
    try:
        connection.autocommit = True  # метод для внесения изменений в БД
        with connection.cursor() as cursor:
            cursor.execute(
                """SELECT * FROM persons;"""
            )
            response = cursor.fetchall()
    except Exception as _ex:
        print("[INFO] Error while working with PostgreSQL", _ex)
    return {'response': response}


@app.get('/persons/some_person')
def get_all_person_data(gender: str = Query(min_length=1, max_length=1), limit: int = Query(gt=0)):
    response = []
    try:
        connection.autocommit = True  # метод для внесения изменений в БД
        if gender.upper() == 'F' or gender.upper() == 'M':
            with connection.cursor() as cursor:
                cursor.execute(
                    f"""SELECT * FROM persons WHERE gender = '{gender.upper()}' LIMIT {limit};"""
                )
                response = cursor.fetchall()
        else:
            response = {f'Gender {gender.upper()} is not valid'}
    except Exception as _ex:
        print("[INFO] Error while working with PostgreSQL", _ex)
    return response


@app.post('/new_person')
def post_json(name: str, gender: str):
    try:
        connection.autocommit = True  # метод для внесения изменений в БД
        with connection.cursor() as cursor:
            cursor.execute(
                f"""INSERT INTO persons (name, gender)
                        VALUES('{name[0].upper()+name[1:]}', '{gender.upper()}');"""
            )
    except Exception as _ex:
        print("[INFO] Error while working with PostgreSQL", _ex)
    return "post method is ready, check PostgreSQL"


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
