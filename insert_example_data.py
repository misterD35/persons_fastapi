import psycopg2
from config import host, user, password, db_name


connection = 0
try:
    connection = psycopg2.connect(  # данные для подключения к базе данных
        host=host,
        user=user,
        password=password,
        database=db_name,
    )

    connection.autocommit = True    # метод для внесения изменений в БД

    with connection.cursor() as cursor:
        cursor.execute(
            """INSERT INTO persons (name, gender)
                    VALUES('Jeremi', 'M'),
                          ('Ann', 'F'),
                          ('Kate', 'F'),
                          ('Richard', 'M'),
                          ('Randy', 'M'),
                          ('Monica', 'F'),
                          ('Peter', 'M'),
                          ('Dabby', 'F'),
                          ('Govard', 'M');"""
        )

        print("[INFO] Data was succefully inserted")


except Exception as _ex:
    print()
    print("[INFO] Error while working with PostgreSQL", _ex)
finally:
    if connection:
        connection.close()
        print()
        print("[INFO] PostgreSQL connection closed")